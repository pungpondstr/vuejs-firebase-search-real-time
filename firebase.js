import firebase from 'firebase'

const firebaseConfig = {
    apiKey: "AIzaSyCSenFGSQgjebImks4hwRULcslBxXrelQY",
    authDomain: "search-realtime.firebaseapp.com",
    projectId: "search-realtime",
    storageBucket: "search-realtime.appspot.com",
    messagingSenderId: "455899023299",
    appId: "1:455899023299:web:40bd804dd4a0c8a4295f76"
};

firebase.initializeApp(firebaseConfig);

export default firebase;